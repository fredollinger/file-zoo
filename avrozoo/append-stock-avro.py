#!/usr/bin/env python3

import os
import numpy as np
import pandas
#import pandavro as pdx

import avrozoo.fiio as fiio

import fastavro

OUTPUT_PATH='stocks.avro'

SCHEMA = {'type': 'record', 'name': 'Root', 'fields': [{'name': 'Date', 'type': ['null', {'type': 'long', 'logicalType': 'timestamp-micros'}]}, {'name': 'Close', 'type': ['null', 'double']}, {'name': 'Ticker', 'type': ['null', 'string']}]}

def make_dict_from_stock(stock: str):
    df = fiio.get_data_web(stock)
    df.reset_index(inplace=True)
    df.drop(['idx'], axis=1, inplace=True)
    # TODO USE WHOLE STOCK
    #df2 = df[0:4]["Close", "Date"]
    df2 = df.loc[0:4, ['Date', 'Close']]
    #print(df2)
    d = {stock : df2.to_dict("records")}
    return d

def get_stock(stock: str):
    df = fiio.get_data_web(stock)
    df.reset_index(inplace=True)
    df.drop(['idx'], axis=1, inplace=True)
    df2 = df.loc[0:4, ['Date', 'Close']]
    df2['Ticker']=stock
    return df2

def main():
    schema = fastavro.parse_schema(SCHEMA)
    stocks = ["AAPL", "TSLA", "FORD", "F"]
    with open(OUTPUT_PATH, 'a+b') as f:
        for st in stocks:
            stock = get_stock(st)
            fastavro.writer(f, schema, stock.to_dict("records"))

if __name__ == '__main__':
    main()
