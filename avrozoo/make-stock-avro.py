#!/usr/bin/env python3

import os
import numpy as np
import pandas
import pandavro as pdx

import avrozoo.fiio as fiio

OUTPUT_PATH='stocks.avro'

#SCHEMA = {'type': 'record', 'name': 'Root', 'fields': [{'name': 'idx', 'type': ['null', 'long']}, {'name': 'Open', 'type': ['null', 'double']}, {'name': 'High', 'type': ['null', 'double']}, {'name': 'Low', 'type': ['null', 'double']}, {'name': 'Close', 'type': ['null', 'double']}, {'name': 'Adj Close', 'type': ['null', 'double']}, {'name': 'Volume', 'type': ['null', 'long']}]}

#schema = {
#    'doc': 'Stock Market Snapshot',
#    'name': 'Stocks',
#    'namespace': 'py3finance',
#    'type': 'record',
#    'fields': [
#        {'name': 'station', 'type': 'string'},
#        {'name': 'time', 'type': 'long'},
#        {'name': 'temp', 'type': 'int'},
#    ],
#}

def make_dict_from_stock(stock: str):
    df = fiio.get_data_web(stock)
    df.reset_index(inplace=True)
    df.drop(['idx'], axis=1, inplace=True)
    # TODO USE WHOLE STOCK
    #df2 = df[0:4]["Close", "Date"]
    df2 = df.loc[0:4, ['Date', 'Close']]
    #print(df2)
    d = {stock : df2.to_dict("records")}
    return d

def get_stock(stock: str):
    df = fiio.get_data_web(stock)
    df.reset_index(inplace=True)
    df.drop(['idx'], axis=1, inplace=True)
    df2 = df.loc[0:4, ['Date', 'Close']]
    df2['Ticker']=stock
    return df2

def main():
    stock = "TSLA"
    df = get_stock(stock)
    print(df)
    pdx.to_avro(OUTPUT_PATH, df)

if __name__ == '__main__':
    main()
