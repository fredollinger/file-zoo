#!/usr/bin/env python3

from pydrill.client import PyDrill

import sys

QUERY = "SELECT Ticker, Close FROM dfs.tmp.`stocks.avro` WHERE Ticker='AAPL' "

drill = PyDrill(host='localhost', port=8047)

if not drill.is_active():
    raise ImproperlyConfigured('Please run Drill first')

query = drill.query(QUERY, timeout=60)
print(query.data)
