#!/usr/bin/env python3

import os
import numpy as np
import pandas as pd
from fastavro import writer, reader, parse_schema

import avrozoo.fiio as fiio

def main():
    with open('stocks.avro', 'rb') as fo:
        for record in reader(fo):
            print(record)

if __name__ == '__main__':
    main()

