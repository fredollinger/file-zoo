#!/usr/bin/env python3

import pandas
import numpy
import yfinance as yf


def clean_stock_data_frame(data):
    data = data.dropna(how="all")
    df = pandas.DataFrame.from_dict(data)
    df = df.set_index("Date")
    return df


def get_data_web(stock: str):
    data = yf.download(stock)
    data = data.dropna(how="all")
    df = pandas.DataFrame.from_dict(data)
    # insert an index into the first row
    # far more complex than I would think
    df.insert(0, 'idx', numpy.arange(len(df)))
    return df


def get_data_local(stock: str):
    return clean_stock_data_frame(pandas.read_csv(f"tests/data/{stock}.csv"))
