#!/usr/bin/env python3

import os
import numpy as np
import pandas as pd

import copy
import json
import avro
from avro.datafile import DataFileWriter, DataFileReader
from avro.io import DatumWriter, DatumReader

with open('stocks.avro', 'rb') as f:
    with DataFileReader(f, DatumReader()) as reader:
        metadata = copy.deepcopy(reader.meta)
        schema_from_file = json.loads(metadata['avro.schema'])
        #json.dump(metadata['avro.schema'], "schema.json")
        #names = [name for name in reader]
        #print(names)

print(f'Schema from users.avro file:\n {schema_from_file}')
